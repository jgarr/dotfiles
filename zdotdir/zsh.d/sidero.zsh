if [ $commands[talosctl] ]; then
  source <(talosctl completion zsh)
fi
if [ $commands[omnictl] ]; then
  source <(omnictl completion zsh)
fi

ealias t=talosctl
ealias o=omnictl
ealias tssh='kubectl -n kube-system debug -it --image http://ghcr.io/nicolaka/netshoot:v0.13 --profile sysadmin node '

# export a talosconfig
et() {
    if [ -n "$1" ]; then
        CONFIG=$(rg --max-depth 3 -l '^context:' $HOME/.talos/ \
            | grep $1)
    else
        CONFIG=$(rg --max-depth 3 -l '^context:' $HOME/.talos/ $PWD | fzf --multi | tr '\n' ':')
    fi
    # echo file and remove trailing :
    echo ${CONFIG%:*}
    export TALOSCONFIG=${CONFIG%:*}
}

# export an omniconfig
eo() {
    if [ -n "$1" ]; then
        CONFIG=$(rg --max-depth 3 -l '^context:' $HOME/.config/omni/ \
            | grep $1)
    else
        CONFIG=$(rg --max-depth 3 -l '^context:' $HOME/.config/omni/ $PWD | fzf --multi | tr '\n' ':')
    fi
    # echo file and remove trailing :
    echo ${CONFIG%:*}
    export OMNICONFIG=${CONFIG%:*}
}

# get clusters from an omni instance
oks-get-clusters() {
    omnictl get clusters -o json | jq -r '.metadata.id'
}

# export the kubeconfig for a cluster
oks-use() {
    if [ -n "$1" ]; then
      KUBECONFIG=$HOME/.kube/$1
      omnictl kubeconfig -f $KUBECONFIG -c $1
    else
      CLUSTER_NAME=$(oks-get-clusters | fzf)
      KUBECONFIG=$HOME/.kube/${CLUSTER_NAME}
      omnictl kubeconfig -f ${KUBECONFIG} -c ${CLUSTER_NAME}
    fi
    # export the kubeconfig
    ek $KUBECONFIG
}
