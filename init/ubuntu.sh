#!/bin/bash

. /etc/os-release

PPAS=(ppa:eosrei/fonts \
	ppa:peek-developers/stable \
	)
APT_PACKAGES=(
	apt-file \
	fonts-twemoji-svginot \
	git \
	git-extras \
	gnome-calculator \
	gnome-tweak-tool \
	htop \
	krita \
	mosh \
	mpv \
	mypaint \
	pavucontrol \
	peek \
	signal-desktop \
	sshfs \
	tmux \
	tree \
	vlc \
	code \
	xclip \
	workspacesclient \
	zsh \
)

# TODO install discord and syncthing

RM_PACKAGES=(snapd)

for ppa in "${PPAS[@]}"; do
	sudo apt-add-repository -y "${ppa}"
done

# slack repo
# TODO fix this
#if ! command -v slack &> /dev/null ; then
#	echo "deb https://packagecloud.io/slacktechnologies/slack/debian/ jessie main" | sudo tee /etc/apt/sources.list.d/slack.list >/dev/null
#fi

# Zoom
if ! command -v zoom &> /dev/null ; then
	wget -O /tmp/zoom.deb https://zoom.us/client/latest/zoom_amd64.deb
	sudo dpkg -i /tmp/zoom.deb
	rm /tmp/zoom.deb
fi

# Signal
if ! command -v signal-desktop &> /dev/null ; then
	curl -s https://updates.signal.org/desktop/apt/keys.asc | sudo apt-key add -
	echo "deb [arch=amd64] https://updates.signal.org/desktop/apt xenial main" | sudo tee -a /etc/apt/sources.list.d/signal-xenial.list
fi

# AWS Workspaces
if [ -f /opt/workspacesclient/workspacesclient ]; then
	wget -q -O - https://workspaces-client-linux-public-key.s3-us-west-2.amazonaws.com/ADB332E7.asc | sudo apt-key add -
	echo "deb [arch=amd64] https://d3nt0h4h6pmmc4.cloudfront.net/ubuntu bionic main" | sudo tee /etc/apt/sources.list.d/amazon-workspaces-clients.list
fi

# VS code
if ! command -v code &> /dev/null; then
	wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
	sudo install -o root -g root -m 644 packages.microsoft.gpg /etc/apt/trusted.gpg.d/
	sudo sh -c 'echo "deb [arch=amd64 signed-by=/etc/apt/trusted.gpg.d/packages.microsoft.gpg] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'
fi

echo "INSTALLING PACKAGES"
echo "###################"
sudo apt update
sudo apt install -y ${APT_PACKAGES[*]}
sudo apt autoremove -y --purge "${RM_PACKAGES[*]}"

echo "UPDATING SETTINGS"
echo "#################"
# Enable fractional scaling
gsettings set org.gnome.mutter experimental-features "['x11-randr-fractional-scaling']"

# Set caps lock as esc key
gsettings set org.gnome.desktop.input-sources xkb-options "['caps:escape']"

# Stupid window grouping
gsettings set org.gnome.desktop.wm.keybindings switch-windows "['<alt>Tab']"
gsettings set org.gnome.desktop.wm.keybindings switch-windows-backward "['<Shift><Alt>Tab', '<Alt>Above_Tab']"
gsettings set org.gnome.desktop.wm.keybindings switch-applications "[]"
gsettings set org.gnome.desktop.wm.keybindings switch-applications-backward "[]"

# Set sloppy window focus
gsettings set org.gnome.desktop.wm.preferences focus-mode 'sloppy'
# Resize window with right click
gsettings set org.gnome.desktop.wm.preferences resize-with-right-button true

## Install homebrew
echo "INSTALLING HOMEBREW"
CI=1
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

echo "INSTALLING DOTFILES"
echo "###################"
# Clone dotfiles
git clone https://gitlab.com/jgarr/dotfiles.git $HOME/.dotfiles

$HOME/.dotfiles/init/dotfiles-install.sh

# install default brew packages
pushd $HOME/.dotfiles/init

brew bundle

popd
