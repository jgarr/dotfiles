#!/bin/bash -x
# ^^ Run from bash assuming we don't already have zsh running

DOTDIR="${DOTFILESDIR:-${HOME}/.dotfiles}"

if [[ ! -d ${DOTDIR} ]]; then
  git clone https://gitlab.com/jgarr/dotfiles.git "${DOTDIR}"
fi

if [[ ! -d /home/linuxbrew/.linuxbrew ]]; then
	# install homebrew
	NONINTERACTIVE=1
	/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
fi

# install brew packages
pushd ${DOTDIR}/init
/home/linuxbrew/.linuxbrew/bin/brew bundle
popd

mkdir -p $HOME/.vim/bundle

if [[ ! -d ~/.vim/bundle/Vundle.vim ]]; then
	# clone vundle before making dotfiles in case .gitconfig requires ssh keys
	git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
fi
# clone theme so vim doesn't error on first run
git clone https://github.com/fatih/molokai.git $HOME/.vim/bundle/molokai

# Update vim plugins
vim -u "${DOTDIR}"/ln/.vimrc +PluginInstall +qall

if [ -d "${DOTDIR}" ]; then
  for DOT_FILE in $( ls -A "${DOTDIR}"/ln ); do
      echo "creating symlink for ${DOT_FILE}"
      ln -s "${DOTDIR}"/ln/"${DOT_FILE}" "${HOME}"/"${DOT_FILE}"
  done
fi

if [[ ! -d ~/.tmux/plugins/tpm ]]; then
	git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
	tmux run-shell ~/.tmux/plugins/tpm/bindings/install_plugins
fi

# run sudo without password
echo "$USER ALL=(ALL) NOPASSWD: ALL" | sudo tee /etc/sudoers.d/$USER
