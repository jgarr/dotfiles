# Enable tab completion
if [ $commands[aws_completer] ]; then
    complete -C $(which aws_completer) aws
fi

# switch aws profile
awsp() {
    # Usage:
    # awsp for interactive prompt
    # awsp $profile to automatically select first match
    if [ -z "$1" ]; then
        export AWS_PROFILE=$(grep profile ${HOME}/.aws/config \
            | awk '{print $2}' \
            | sed 's,],,g' \
            | fzf --layout reverse --height=10% --border)
    else
        export AWS_PROFILE=$(grep profile ${HOME}/.aws/config \
            | awk '{print $2}' | sed 's,],,g' \
            | grep -i "$1" \
            | head)
        if [ -n "$2" ]; then
            awsr "$2"
        fi
    fi
    export AWS_DEFAULT_PROFILE=$AWS_PROFILE
}

ealias PDX=us-west-2
ealias IAD=us-east-1
ealias CMH=us-east-2
ealias SFO=us-west-1
ealias DUB=eu-west-1
ealias LHR=eu-west-2

# export aws region
awsr() {
    # Usage:
    # awsr for interactive prompt
    # awsr $region to automatically select first match
    local REGIONS=$(cat $HOME/.aws/regions.txt)
    if [ -z "$1" ]; then
        export AWS_REGION=$(printf $REGIONS \
            | fzf --layout reverse --height=10% --border \
            | awk -F':' '{print $1}' \
            | head -n1)
    else
        export AWS_REGION=$(printf $REGIONS \
            | grep -i "$1" \
            | awk -F':' '{print $1}' \
            | head -n1)
    fi
    export AWS_DEFAULT_REGION=$AWS_REGION
}

# copy aws account number
awsa() {
    grep -B1 --no-group-separator iam ${HOME}/.aws/credentials \
        | cut -d: -f5 | tr ']\n' ' ' | tr '[' '\n' \
        | column -t | fzf --height 10% --border \
        | tee /dev/tty | awk '{print $2}' \
        | xclip -selection clipboard
}

# unset AWS_*
awsu() {
    unset AWS_PROFILE \
        AWS_DEFAULT_PROFILE \
        AWS_REGION \
        AWS_DEFAULT_REGION
}

# print current AWS_* variables
awse() {
    env | grep --color=never AWS_
}

aws-asg-counts() {
  aws autoscaling  describe-auto-scaling-groups --auto-scaling-group-names $1 | jq .AutoScalingGroups[].Instances[].AvailabilityZone | sort | uniq -c
}

# SSH to instance via instance ID
sshi() {
    if [[ ${1} = i-* ]]; then
        ssh ec2-user@$(aws ec2 describe-instances \
            --instance-ids ${1:-$INSTANCEID} \
            --query 'Reservations[].Instances[].NetworkInterfaces[].PrivateIpAddress' \
            --output text) $@
    else
        ssh $(aws ec2 describe-instances \
          --filter "Name=tag:Name,Values=${1}" \
          --output text \
          --query 'Reservations[0].Instances[0].InstanceId') "${@:2}"
    fi
}

# Start session manager to instance via ID
ssmi() {
    aws ssm start-session --target ${1:-INSTANCEID}
}
# Start session manager to instance via private IP
ssmip() {
    aws ssm start-session --target $(aws ec2 describe-instances --filter Name=private-ip-address,Values=${1} --query 'Reservations[].Instances[].[InstanceId]' --output text)
}
# Give me a dev vm in aws
# exports $INSTANCEID for ssmi and sshi functions
alias aws-devm='eval $(create-instance)'

# Mark instance unhealthy
alias aws-set-unhealthy='aws autoscaling set-instance-health --health-status Unhealthy --instance-id '

# Get tags on an instance
function aws-get-tags() {
    aws ec2 describe-tags \
        --filters "Name=resource-id,Values=${1:$INSTANCE-ID}" "Name=key,Values=${TAG_PREFIX}*"  \
        --output text
}

alias aws-get-console='aws ec2 get-console-output --output text --latest --instance-id '

eks-console-access() {
    set -x
    aws-iam-authenticator add role --rolearn arn:aws:iam::$(aws whoami account):role/rothgar --username rothgar --groups system:masters --kubeconfig ${KUBECONFIG}
}

eks-write-conf() {
    if [ -n "$2" ]; then
        echo 'export AWS_REGION='"${2}"
        AWS_REGION=${2} aws eks update-kubeconfig --name "${1}" --kubeconfig $HOME/.kube/"${1}" --alias "${1}" > /dev/null
    else
	    aws eks update-kubeconfig --name "${1}" --kubeconfig $HOME/.kube/"${1}" --alias "${1}" > /dev/null
    fi
	echo 'export KUBECONFIG=$HOME/.kube/'"${1}"
}

eks-get-clusters() {
    # EKS attributes https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eks_cluster#attributes-reference
	if [[ $# > 0 ]]; then
		awsls aws_eks_cluster --regions us-west-1,us-west-2,us-east-1,us-east-2,eu-west-1,ca-central-1,eu-central-1,eu-west-2,eu-west-3 -a version \
			| awk '/^$/ {next}; {$1=""; print $0}' \
			| grep ${1} \
            | head -n 1 \
			| awk '{print $1,$3,$5}' \
			| column -t -s " "
	else
		awsls aws_eks_cluster --regions us-west-1,us-west-2,us-east-1,us-east-2,eu-west-1,ca-central-1,eu-central-1,eu-west-2,eu-west-3 -a version \
			| awk '/^$/ {next}; {$1=""; print $0}' \
			| awk '{print $1,$3,$5}' \
			| column -t -s " "
	fi
}

eks-use() {
	eks-write-conf $(eks-get-clusters ${1} | fzf --header-lines=1 | awk '{print $1}')
}

eks-oidc() {
    aws eks describe-cluster --name $(eks-get-clusters ${1} | fzf --header-lines=1 | awk '{print $1}') --query "cluster.identity.oidc.issuer" --output text | cut -d '/' -f 5
}

eks-endpoint() {
    aws eks describe-cluster --name $(eks-get-clusters ${1} | fzf --header-lines=1 | awk '{print $1}') --query "cluster.endpoint" --output text
}

eks-upgrade() {
	if [[ $# > 0 ]]; then
        local CLUSTER_NAME=$(eks-get-clusters ${1} | awk '{print $1}')
    else
        local CLUSTER_NAME=$(eks-get-clusters | fzf --header-lines=1 | awk '{print $1}')
    fi
    local K8S_VERSION=$(eks-get-clusters ${CLUSTER_NAME} | awk '{print $3}' | cut -d '.' -f2)
    local NEXT_K8S_VERSION=$(( K8S_VERSION +1 ))
    aws eks update-cluster-version --name $CLUSTER_NAME --kubernetes-version "1.${NEXT_K8S_VERSION}"
}

get-ec2-userdata() {
    aws ec2 describe-instance-attribute --instance-id "${1}" --attribute userData --query 'UserData.Value' --out text | base64 -d
}
