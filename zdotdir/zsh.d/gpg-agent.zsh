# gpg-agent setup                                                               
function _start_gpg_agent {
  gpg-agent --daemon \
  --write-env-file "${HOME}/.gpg-agent-info" > /dev/null 2>&1
  . "${HOME}/.gpg-agent-info"
}

if [ -f "${HOME}/.gpg-agent-info" ]; then
  source "${HOME}/.gpg-agent-info"

  export GPG_AGENT_INFO
else
  _start_gpg_agent
fi
export GPG_TTY=$(tty)
