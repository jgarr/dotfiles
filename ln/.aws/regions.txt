us-east-1:US East:(N. Virginia):IAD
us-east-2:US East:(Ohio):CMH
us-west-1:US West:(N. California):SFO
us-west-2:US West:(Oregon):PDX
af-south-1:Africa:(Cape Town):CPT
ap-east-1:Asia Pacific:(Hong Kong):HKG
ap-south-1:Asia Pacific:(Mumbai):BOM
ap-northeast-3:Asia Pacific:(Osaka-Local):KIX
ap-northeast-2:Asia Pacific:(Seoul):ICN
ap-southeast-1:Asia Pacific:(Singapore):SIN
ap-southeast-2:Asia Pacific:(Sydney):SYD
ap-northeast-1:Asia Pacific:(Tokyo):NRT
ca-central-1:Canada:(Montreal):YUL
eu-central-1:Europe:(Frankfurt):FRA
eu-west-1:Europe:(Ireland):DUB
eu-west-2:Europe:(London):LHR
eu-south-1:Europe:(Milan):MXP
eu-west-3:Europe:(Paris):CDG
eu-north-1:Europe:(Stockholm):ARN
me-south-1:Middle East:(Bahrain):BAH
sa-east-1:South America:(São Paulo):GRU
