export ZDOTDIR=$HOME/.dotfiles/zdotdir

alias assume="source assume"
if [[ -d $HOME/.cargo/env ]]; then
    . "$HOME/.cargo/env"
fi
