#!/bin/bash -x

# vscodeium repo
if ! [ -x "$(command -v codeium)" ]; then
	wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg | sudo apt-key add -
	echo 'deb https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/repos/debs/ vscodium main' | sudo tee /etc/apt/sources.list.d/vscodium.list
fi

# xpra repo
if ! [ -x "$(command -v xpra)" ]; then
	wget -q https://xpra.org/gpg.asc -O- | sudo apt-key add -
	echo 'deb https://xpra.org/ buster main' | sudo tee /etc/apt/sources.list.d/xpra.list
fi
	
# Signal
if ! [ -x "$(command -v signal-desktop)" ]; then
	curl -s https://updates.signal.org/desktop/apt/keys.asc | sudo apt-key add -
	echo "deb [arch=amd64] https://updates.signal.org/desktop/apt xenial main" | sudo tee /etc/apt/sources.list.d/signal-xenial.list
fi

APT_PACKAGES=( 
	apt-transport-https \
	codium \
	fasd \
	git \
	htop \
	iputils-arping \
	iputils-ping \
	iputils-tracepath \
	krita \
	libssl-dev \
	mosh \
	mtr \
	mypaint \
	neovim \
	peek \
	signal-desktop \
	sshfs \
	tmux \
	tree \
	wget \
	xpra \
	zsh \
)

apt update
apt install -y "${APT_PACKAGES[@]}"

CURRENT_LOGIN_SHELL=$(getent passwd $USER | cut -d: -f7)
if [[ "${CURRENT_LOGIN_SHELL}" != *"zsh"* ]]; then
	chsh -s /usr/bin/zsh
fi
