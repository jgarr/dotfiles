#!/bin/bash

. /etc/os-release

PPAS=(ppa:eosrei/fonts)
APT_PACKAGES=(
	apt-file \
	fonts-twemoji-svginot \
	git \
	git-extras \
	htop \
	mosh \
	sshfs \
	tmux \
	tree \
	code \
	zsh \
)

RM_PACKAGES=(snapd)

for ppa in "${PPAS[@]}"; do
	sudo apt-add-repository -y "${ppa}"
done

# VS code
if ! command -v code &> /dev/null; then
	wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
	sudo install -o root -g root -m 644 packages.microsoft.gpg /etc/apt/trusted.gpg.d/
	sudo sh -c 'echo "deb [arch=amd64 signed-by=/etc/apt/trusted.gpg.d/packages.microsoft.gpg] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'
fi

sudo apt update
sudo apt install -y ${APT_PACKAGES[*]}
sudo apt autoremove -y --purge "${RM_PACKAGES[*]}"

# install default brew packages
pushd $HOME/.dotfiles/init

brew bundle

popd
