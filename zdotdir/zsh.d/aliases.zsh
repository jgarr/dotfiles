#------------ALIASES------------------

# Directory listing

if command -v eza >& /dev/null; then
    # show git status in ll
    function ll() {
        if [ -d .git ] || git rev-parse --git-dir > /dev/null 2>&1 ; then
            eza -alFh --git $@
        else
            eza -alFh $@
        fi
    }
    ealias la='eza -1a'
    ealias l='eza -F'
    ealias latr='eza -s size -l --group-directories-first'
    ealias lla='eza -a'
    ealias l1='eza -1'       # All files in 1 column
    ealias tree='eza -T'
else
    ealias ll='ls -AlFh'
    ealias la='ls -A'
    ealias l='ls -CF'
    ealias latr='ls -lAtr'
    ealias lla='ls -A'
    ealias l1='ls -1A'       # All files in 1 column
    ealias tree='tree -F'
fi

if command -v bat >& /dev/null; then
    alias cat=bat
fi

# Easier navigation: .., ..., -
alias ..='cd ..'
alias ....='cd ../..'
alias xxx='exit'
alias :q='exit'
alias -- --='cd -'
alias dl="cd ~/Downloads"
alias src="cd ~/src"
# allow copy/paste of tutorials with leading $
alias '$'=''

if command -v apt >& /dev/null; then
    alias upgrade='sudo apt update && sudo apt upgrade -y'
elif command -v yum >& /dev/null; then
    alias upgrade='sudo yum update -y'
fi

if [ "$TERM" = 'xterm-kitty' ]; then
    alias ssh="kitty +kitten ssh"
fi

# View HTTP traffic
alias sniff="sudo ngrep -d 'en1' -t '^(GET|POST) ' 'tcp and port 80'"
alias httpdump="sudo tcpdump -A -s 10240 'tcp port 4080 and (((ip[2:2] - ((ip[0]&0xf)<<2)) - ((tcp[12]&0xf0)>>2)) != 0)' \
    | egrep --line-buffered \"^........(GET |HTTP\/|POST |HEAD )|^[A-Za-z0-9-]+: \" \
    | sed -r 's/^........(GET |HTTP\/|POST |HEAD )/\n\1/g'"

# hosts
ealias hosts='sudo vim /etc/hosts'

# Give me sudo access without a password
ealias sudome='echo "$USER ALL=(ALL) NOPASSWD:ALL" | sudo tee /etc/sudoers.d/$USER'
ealias dockerme='sudo usermod -aG docker $USER'

# File size
ealias fs="stat -f '%z bytes'"
ealias df="df -h"
ealias du='du -ch'

if (( $+commands[subscription-manager] )) ; then
    ealias sm="sudo subscription-manager"
fi

# MISC commands
ealias g='git'
ealias gr='cd $(git rev-parse --show-toplevel)'
ealias psg="ps aux | grep -v grep | grep -i -e VSZ -e"
ealias lsg="sudo lsof | grep"
ealias suod='sudo'

export EDITOR='vim'
export VISUAL='vim'

ealias v='nvim'
ealias sv='sudo vim'

ealias lmsg='sudo less +F /var/log/messages'
ealias dmsg='less +F /var/log/dmesg'
alias -g H='| head'
alias -g L='| less'
alias -g G='| grep'
alias -g W='| wc -l'
alias -g J='| jq .'
alias -g T="| tr -d '\n' "
alias -g C="| xclip -selection clipboard"

ealias df="df -h"
ealias rpg="rpm -qa | grep -i"
ealias ylist='yum list --showduplicates'
ealias ygroups="yum groupinfo '*' | less +/"
alias curl-trace='curl -w "@${HOME}/.dotfiles/ln/curl-format" -o /dev/null -s'
alias open='xdg-open'
# Let me use aliases with watch command
alias watch="watch "

# *ctl
ealias sc='sudo systemctl'
ealias scs='systemctl status'
ealias scr='sudo systemctl restart'
ealias scc='systemctl cat'
ealias jc='sudo journalctl'
ealias jcf='sudo journalctl -flu'
ealias mc='sudo machinectl'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# Show top 10 commands
ealias history-stat="history 0 | awk '{print \$2}' | sort | uniq -c | sort -n -r | head"

# Generic colorizer
[[ -f "/etc/grc.zsh" ]] && source /etc/grc.zsh
