#!/bin/bash

dpkg --merge-avail <(apt-cache dumpavail)
dpkg --clear-selections
dpkg --set-selections < packages.txt
apt deselect-upgrade
