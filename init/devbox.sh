#!/bin/bash

. /etc/os-release

PPAS=(ppa:eosrei/fonts)
APT_PACKAGES=(
	apt-file \
	fonts-twemoji-svginot \
	git \
	git-extras \
	htop \
	mosh \
	tmux \
	tree \
	zsh \
)

RM_PACKAGES=(snapd)

for ppa in "${PPAS[@]}"; do
	sudo apt-add-repository -y "${ppa}"
done

sudo apt update
sudo apt install -y ${APT_PACKAGES[*]}
sudo apt autoremove -y --purge "${RM_PACKAGES[*]}"

# import public ssh key from github
ssh-import-id gh:rothgar

# clone dotfiles
curl -sSfl 'https://gitlab.com/jgarr/dotfiles/-/raw/main/init/dotfiles-install.sh' | bash

sudo chsh -s /usr/bin/zsh ubuntu
